# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class UpdateHistory(models.Model):
    userID = models.IntegerField()
    itemID = models.IntegerField()
    date = models.DateTimeField('date published')
    
    def __str__(self):
        return "Item {} updated from {} at {}".format(str(self.itemID),str(self.userID),self.date)

class Item(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    filepath = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class LoginNames(models.Model):
    userID = models.IntegerField()
    inTouch = models.CharField(max_length=200)
    unitClub = models.CharField(max_length=200)
