from django.conf.urls import url

from . import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView


app_name = 'updater'
urlpatterns = [
    url(r'^$', login_required(views.index), name='index'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^signup/$', login_required(views.signup), name='signup'),
    url(r'^changeUser/$', login_required(views.UserUpdate.as_view(template_name='updater/changeUser.html')), name='changeUser'),
    url(r'^changePWD/$', login_required(views.changePWD), name='changePWD'),
    url(r'^info/$', login_required(TemplateView.as_view(template_name='updater/information.html')), name='info'),
    url(r'^infoaccept/$', login_required(views.infoaccept), name='infoaccept'),
    # url(r'^settings/$', login_required(views.settings), name='settings'),
    url(r'^(?P<item_id>[0-9]+)/(?P<user_id>[0-9]+)/refresh/$', login_required(views.refresh), name='refresh'),
    url(r'^(?P<user_id>[0-9]+)/sessionIntouch/$', login_required(views.getSessionIntouch), name='sessionIntouch'),
    url(r'^(?P<user_id>[0-9]+)/sessionUnitClub/$', login_required(views.getSessionUnitClub), name='sessionUnitClub'),

]
