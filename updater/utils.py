import pickle
def save_cookies(requests_cookiejar, filename):
    with open(filename, 'wb') as f:
        pickle.dump(requests_cookiejar, f)

def load_cookies(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)

startURLIntouch = 'https://www.marykayintouch.de'
loginUrlIntouch = 'https://www.marykayintouch.de/Login/Login.aspx'

# 'Adressen und Karrierestufen'
adress_url = 'http://www.marykayintouch.de/InTouchWeb/myBusiness/api/Member/GetMemberData?dataType=Unit&_=14684277701' #&_=1468427770186

# define header for excel file
header = ('Linie',
          'Unit',
          'Länderkennzeichen',
          'Consultantnummer',
          'Nachname',
          'Vorname',
          'Aktivitätsstatus',
          'Startdatum',
          'Geburtsdatum',
          'Level',
          'Datum letzter Auftrag',
          'Warenwert letzte Bestellung',
          'Straße',
          'Straße 2',
          'Stadt',
          'Staat',
          'PLZ',
          'Land',
          'Mobilnummer',
          'Festnetznummer',
          'Arbeit',
          'E-Mail',
          'Anwerber/in',
          'Anwerber/in Nachname',
          'Anwerber/in Vorname',
          # 'Startdatum',
          # 'Datum letzter Auftrag',
          # 'Warenwert letzte Bestellung',
          'Level ID')

# LadderOfSuccess Current Quater
LadderOfSuccessURL = ['http://www.marykayintouch.de/InTouchWeb/reports/api/Report?id=ladder-of-success-current-quarter-unit&_productionDateApiParameter=ProductionQuarter&parameters=%7B%22ProductionQuarter%22%3A%222017-07-30T23%3A59%3A59%22%7D&setProductionDate=undefined&_=1494279924998',
            'http://www.marykayintouch.de/InTouchWeb/reports/api/Report?id=ladder-of-success-current-quarter-unit&data=ReportSeminarQuarters&_productionDateApiParameter=ProductionQuarter&parameters=%7B%22ProductionQuarter%22%3A%222017-07-30T23%3A59%3A59%22%7D&setProductionDate=undefined&_=1494450108888' ]


ladderOfSuccessTMPL = '''FIRST
"Unit-Mitglieder","Erreichte Stufe","Unit-Mitglieder","Erreichte Stufe","Unit-Mitglieder","Erreichte Stufe","Unit-Mitglieder","Erreichte Stufe"
SECOND
"""
""Marketingplan","Consultant-Nummer","Name","Geschäftsstufe","Anzahl neue Mitglieder","Anzahl qualifizierte Mitglieder","Umsatz inkl. Bonus","Erreichte Stufe","Fehlt zum nächsten Steinchen"
THIRD
""
'''

url = [
# 0 Worked
'http://www.marykayintouch.de/InTouchWeb/Reports/Report.aspx?Path=/Sales_and_Recruiting/MonthlySales',
# 1 Broken Link
# 'http://www.marykayintouch.de/InTouchWeb/Reports/Report.aspx?Path=/Contest_and_Challenges/LadderOfSuccessCurrentQuarter',
# 2 Worked
'http://www.marykayintouch.de/InTouchWeb/Reports/Report.aspx?Path=/Contest_and_Challenges/LadderOfSuccessPreviousQuarter',
# 3 Worked
'http://www.marykayintouch.de/InTouchWeb/Reports/Report.aspx?Path=/Sales_and_Recruiting/TeamAnalysisSales',
# 4 
'http://www.marykayintouch.de/InTouchWeb/Reports/Report.aspx?Path=/Monthly_and_Historical/RecruitersCommission',
]

# PeopleAndAdresses - Adressen Und Karrierestufen
# Monats-Umsatz - MonthlySales.xls
# LadderofSuccessPreviousQuarter - Erfolgsleiter vergengenes Quartal 
# TeamAnalysisSales - Team Umsatz
# RecruitersCommission - Team Provision
# LadderOfSuccessCurrentQuarter - Erfolgsleiter aktuelles Quartal 
# LadderOfSuccessPreviousQuarter - Erfolgsleiter vergangenes Quartal 

CallingName = ['Monats-Umsatz',
               'Erfolgsleiter vergangenes Quartal', 
               'Team Umsatz',
               'Team Provision',
               'Erfolgsleiter aktuelles Quartal',
               'Adressen Und Karrierestufen'
               ]

    # 0 - MonthlySales
    # 1 - LadderOfSuccessPreviousQuarter
    # 2 - TeamAnalysisSales
    # 3 - RecruitersCommission
    # 4 - LadderOfSuccessCurrentQuarter
    # 5 - PeopleAndAdresses


fileName = [
'MonthlySales.xls',
# 'files/LadderOfSuccessCurrentQuarter.xls',
'LadderOfSuccessPreviousQuarter.xls',
'TeamAnalysisSales.xls',
'RecruitersCommission.xls',
]
payload = [
{
    # 0
    'ctl00$ContentPlaceHolder1$MonthlySales$monthly-sales-unit$ViewPanelHeader$CommandBar$CommandExport$ctl00': 'Übersicht exportieren',
    'ctl00$ContentPlaceHolder1$MonthlySales$monthly-sales-unit$ViewPanelHeader$ConfigFilter$CareerLevel$CareerLevel': 'Alle',
    'ctl00$ContentPlaceHolder1$MonthlySales$monthly-sales-unit$ViewPanelHeader$GridColumnPager1$ctl01$ctl00': 'Retail'
    # Get this load dynamically from the website
    # 'ctl00$ContentPlaceHolder1$MonthlySales$monthly-sales-unit$ViewPanelHeader$ConfigFilter$ProductionYear$ProductionYear':
},
{
    # 1
    'ctl00$ContentPlaceHolder1$LadderOfSuccessPreviousQuarter$ladder-of-success-previous-quarter-unit$ViewPanelHeader$CommandBar$CommandExport$ctl00': 'Übersicht exportieren',
    # Get this load dynamically from the website
    # 'ctl00$ContentPlaceHolder1$LadderOfSuccessPreviousQuarter$ladder-of-success-previous-quarter-unit$ViewPanelHeader$ConfigFilter$QuarterStart$QuarterStart',
    'ctl00$ContentPlaceHolder1$LadderOfSuccessPreviousQuarter$ladder-of-success-previous-quarter-unit$ViewPanelHeader$ConfigFilter$ReportingSubsidiaryCode$ReportingSubsidiaryCode' : 'All',
},
{
    # 2
    'ctl00$ContentPlaceHolder1$TeamAnalysisSales$team-analysis-sales-unit$ViewPanelHeader$CommandBar$CommandExport$ctl00': 'Übersicht exportieren',
    'ctl00$ContentPlaceHolder1$TeamAnalysisSales$team-analysis-sales-unit$ViewPanelHeader$ConfigFilter$CareerLevel$CareerLevel': 'Nicht Terminierte',
    # Get this load dynamically from the website
    # 'ctl00$ContentPlaceHolder1$TeamAnalysisSales$team-analysis-sales-unit$ViewPanelHeader$ConfigFilter$ProductionYear$ProductionYear'
    'ctl00$ContentPlaceHolder1$TeamAnalysisSales$team-analysis-sales-unit$ViewPanelHeader$GridColumnPager1$ctl01$ctl00': 'Retail'
},
{
    # 3
    'ctl00$ContentPlaceHolder1$RecruitersCommission$RecruitersCommissionUnit$ViewPanelHeader$CommandBar$CommandExport$ctl00':'Übersicht exportieren',
    # Get this load dynamically from the website
    # 'ctl00$ContentPlaceHolder1$RecruitersCommission$RecruitersCommissionUnit$ViewPanelHeader$ConfigFilter$ProductionMonth$ProductionMonth':
    'ctl00$ContentPlaceHolder1$RecruitersCommission$RecruitersCommissionUnit$ViewPanelHeader$ConfigFilter$ReportingSubsidiaryCode$ReportingSubsidiaryCode': 'All'


    # 'ctl00$ContentPlaceHolder1$RecruitersCommission$RecruitersCommissionUnit$ViewPanelHeader$CommandBar$CommandExport$ctl00': 'Übersicht exportieren',
    # 'ctl00$ContentPlaceHolder1$RecruitersCommission$RecruitersCommissionUnit$ViewPanelHeader$ConfigFilter$ProductionMonth$ProductionMonth' : '01.06.2016 00:00:00',
    # 'ctl00$ContentPlaceHolder1$RecruitersCommission$RecruitersCommissionUnit$ViewPanelHeader$ConfigFilter$ReportingSubsidiaryCode$ReportingSubsidiaryCode': 'All'
}
]