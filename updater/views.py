from __future__ import unicode_literals

import datetime
import io
import json
import os
import requests

import updater.utils as utils

from django.views.generic.edit import UpdateView

from .forms import UpdateProfile
from .models import LoginNames
from .models import UpdateHistory

from bs4 import BeautifulSoup
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone

    # 0 - MonthlySales
    # 1 - LadderOfSuccessPreviousQuarter
    # 2 - TeamAnalysisSales
    # 3 - RecruitersCommission
    # 4 - LadderOfSuccessCurrentQuarter
    # 5 - PeopleAndAdresses
    # 6 - InTouchSession
    # 7 - UnitClub
    
# Timeout for requests in seconds 
TIMEOUT = 60

def getSessionIntouch(request,user_id):

    item_id = 6
    loginload = {
        'txtConsultantID': request.POST.get('username'),
        'txtPassword': request.POST.get('password'),
        '__EVENTTARGET': 'btnSubmit'
    }

    try: 
        loginName = LoginNames.objects.get(userID = int(user_id) )
        loginName.inTouch = request.POST.get('username')
        loginName.save()
        print('Saved new login name for Intouch for user {}'.format(user_id))
    except:
        new = LoginNames(userID=int(user_id), inTouch=request.POST.get('username'))
        new.save()     
        print('New Intouch LoginName created for user {}'.format(user_id))
   

    print(loginload)
    cookieIntouchPath = 'cookieIntouch.dat'

    userpath = os.path.join(settings.BASE_DIR,'updater','files',str(user_id))
    if not os.path.isdir(userpath):
            os.makedirs(userpath)

    BackGroundInput = dict()

    # return HttpResponseRedirect(reverse('updater:index'))

    with requests.session() as c:
        # get the start cookies of the session
        try:
            html = c.get(utils.startURLIntouch, timeout = TIMEOUT)
        except requests.exceptions.Timeout:
            messages.error(request, 'In Touch reagiert leider nicht. Bitte versuche \
                                        es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return HttpResponseRedirect(reverse('updater:index'))

        soup = BeautifulSoup(html.text, "lxml")

        # find all inputs to submit even the hidden ones
        for elem in soup.find('form').find_all('input', type='hidden'):
            BackGroundInput[elem['id']] = elem['value']

        # login to site with loginload as POST data
        # print('Login in to MaryKay Intouch')

        BackGroundInput.update(loginload)
        response = c.post(utils.loginUrlIntouch, data=BackGroundInput)
        hiddenResponse = c.get(utils.startURLIntouch)
        BackGroundInput.clear()

        if '.ASPXAUTH' in c.cookies.keys():
            # print(c.cookies['.ASPXAUTH'])
            utils.save_cookies(c.cookies,os.path.join(userpath, cookieIntouchPath))
            # print('saved cookies')
            cookieIntouch = c.cookies
            b = UpdateHistory(userID=int(user_id), itemID=item_id, date=timezone.now())
            b.save()
            messages.info(request, 'Login war erfolgreich!')

        else: 
            messages.error(request, 'Login hat nicht funktioniert!')
            # print('Login hat nicht funktioniert!')

    return HttpResponseRedirect(reverse('updater:index'))

def getSessionUnitClub(request,user_id):

    try: 
        loginName = LoginNames.objects.get(userID = int(user_id))
        loginName.unitClub = request.POST.get('username')
        loginName.save()
        print('Saved new login name for unit Club for user {}'.format(user_id))
    except:
        new = LoginNames(userID=int(user_id), unitClub=request.POST.get('username'))
        new.save()   
        print('New Unit Club LoginName created for user {}'.format(user_id))
     

    userpath = os.path.join(settings.BASE_DIR,'updater','files',str(user_id))
    if not os.path.isdir(userpath):
            os.makedirs(userpath)

    # Unit-Club
    loginload = {
        'uid': request.POST.get('username'),
        'pwd': request.POST.get('password')
    }
    cookieUnitClubPath = 'cookieUnitClub.dat'

    loginUrl = 'http://www.unit-club.net/up/unitclub/_login.php?' 
    # return HttpResponseRedirect(reverse('updater:index'))

    with requests.session() as c:
        # log in to website and get all the cookies
        print('getting new session')
        try:
            loginResponse = c.post(loginUrl, data=loginload, timeout = TIMEOUT)
        except requests.exceptions.Timeout:
            messages.error(request, 'Der Unit Club reagiert leider nicht. Bitte versuche \
                                        es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return HttpResponseRedirect(reverse('updater:index'))

        soup = BeautifulSoup(loginResponse.text, "lxml")
        mydivs = soup.findAll("span", { "class" : "errormeldung" })

        if mydivs:
            print('Login nicht korrekt')
            messages.error(request, 'Login hat nicht funktioniert!')
        else:
            print('Login korrekt')
            messages.info(request, 'Login war erfolgreich!')
            cookieUnitClub = c.cookies
            print('Saving Cookie')
            utils.save_cookies(cookieUnitClub, os.path.join(userpath, cookieUnitClubPath))
            item_id = 7
            b = UpdateHistory(userID=int(user_id), itemID=item_id, date=timezone.now())
            b.save()

    return HttpResponseRedirect(reverse('updater:index'))

def logout_view(request):
    logout(request)
    return render(request, 'registration/logout.html')

def index(request):

    current_user = request.user
    s = User.objects.get(id=current_user.id)

    if not s.last_login:
        return render(request, 'updater/information.html')

    content = {}

    try:
        loginNames = LoginNames.objects.get(userID=current_user.id)
        print('Got one')
        print(loginNames)
    except:
        print('Got None')
        loginNames = ''
    
    content['loginNames'] = loginNames
    updateTime = [0 for x in range(8)]

    for kk in range(8):
        tmpTime =  UpdateHistory.objects.order_by('-date').filter(userID=request.user.id,itemID=kk)
        if not tmpTime:
            updateTime[kk] = 0
            continue
        t = timezone.now()-tmpTime[0].date
        s = t.total_seconds()

        if (kk == 6) or (kk == 7):
            if s > 10800:
                updateTime[kk] = False
                continue
        updateTime[kk] = '{}:{}:{}'.format(int(s // 3600), int(s % 3600 // 60), int(s % 60))

    content['updateTime'] = updateTime
    # print(content)
    return render(request, 'updater/index.html', content)

def refresh(request,item_id,user_id):

    itemID = int(item_id)
    print(user_id)
    print(itemID)
    print('Updating {}'.format(utils.CallingName[itemID]))

    if int(itemID) <= 3:

        try:
            inTouchRest(request,user_id,itemID)
            # print('Uploading {}'.format(utils.CallingName[itemID]))
            unitClubUpload(request,user_id,itemID)
            b = UpdateHistory(userID=int(user_id), itemID=int(item_id), date=timezone.now())
            b.save()
            messages.info(request, 'Aktualisierung war erfolgreich!')
        except:
            messages.error(request, 'Aktualisierung ist leider fehlgeschlagen!')

    elif itemID==4:
        try:
            # print('Downloading Erfolgsleiter aktuelles Quartal')
            inTouchLadderofSuccess(request,user_id)
            # print('Uploading Erfolgsleiter aktuelles Quartal')
            unitClubUpload(request,user_id,itemID)
            b = UpdateHistory(userID=int(user_id), itemID=int(item_id), date=timezone.now())
            b.save()
            messages.info(request, 'Aktualisierung war erfolgreich!')
        except:
            messages.error(request, 'Aktualisierung ist leider fehlgeschlagen!')

    elif itemID==5:
        try:
            print('Uploading Adressen und Karriereleiter')
            inTouchPeopleAndAdresses(request,user_id)
            unitClubUpload(request,user_id,itemID)
            b = UpdateHistory(userID=int(user_id), itemID=int(item_id), date=timezone.now())
            b.save()
            messages.info(request, 'Aktualisierung war erfolgreich!')
        except:
            messages.error(request, 'Aktualisierung ist leider fehlgeschlagen!')


    return HttpResponseRedirect(reverse('updater:index'))

def infoaccept(request):
    #Puts current date in for new user

    current_user = request.user
    s = User.objects.get(id=current_user.id)
    s.last_login = timezone.now()
    s.save()

    return render(request, 'updater/index.html')

def changePWD(request):
    # If yout want to change the password
    if request.method == 'POST':
        formPasswd = PasswordChangeForm(request.user, request.POST)
        if formPasswd.is_valid():
            user = formPasswd.save()
            update_session_auth_hash(request, user)  # Important!
            print('Changing password')
            messages.info(request, 'Dein Passwort wurde erfolgreich geändert!')
            return redirect('updater:changePWD')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        formPasswd = PasswordChangeForm(request.user)

    return render(request, 'updater/changePWD.html', {
        'formPasswd': formPasswd,
    })

def signup(request):

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            messages.info(request,'Dein Account {} wurde erfolgreich erstellt.'.format(username))
            return redirect('/')
    else:
        form = UserCreationForm()
    print(form)
    return render(request, 'updater/signup.html', {'form': form})

class UserUpdate(UpdateView):
    model = User
    fields = ['username', 'email', 'first_name', 'last_name']

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        messages.info(self.request,'Deine Informationen wurden erfolgreich geändert.')
        return reverse('updater:changeUser')

# Helpers


def inTouchPeopleAndAdresses(request,user_id):
    import dateutil.parser as dparser
    import xlwt

    print('Checking Cookie')

    if not checkCookie('InTouch',user_id):
        return 'error'

    print('Downloading AdressListe!')

    # excelData = tablib.Dataset()
    # excelData.append(utils.header)
    excelPath = os.path.join(getUserPath(user_id),'PeopleAndAdresses.xls')

    wb = xlwt.Workbook()
    ws = wb.add_sheet('Sheet1')

    for jj, elem in enumerate(utils.header):
        ws.write(0,jj,elem)

    BackGroundInput = dict()
    print('Loading Cookie')
    cookieIntouch = load_cookies(os.path.join(getUserPath(user_id),'cookieIntouch.dat'))

    with requests.session() as c:
        # GET 'Adressen und Karrierestufen'
        # print('Getting the Adresses List')
        try:
            adresses = c.get(utils.adress_url, cookies = cookieIntouch, timeout=TIMEOUT)
            print(str(adresses.status_code))
        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'

        # Check if the request was accepted
        if not adresses.status_code == 200:
            print('Access forbidden!')
            return

        json_format = BeautifulSoup(adresses.text, 'lxml').get_text()
        json_data = json.loads(json_format)

        with open(os.path.join(getUserPath(user_id),'tmp.dat'),'w') as f:
                json.dump(json_data,f)

        for ii, elem in enumerate(json_data):
            # excelData.append(
            #     ['', elem['UnitNumber'],
            #      elem['SubsidiaryCode'],
            #      elem['ConsultantID'],
            #      elem['LastName'],
            #      elem['FirstName'],
            #      elem['ConsultantStatus'],
            #      dparser.parse(elem['StartDate'], fuzzy=True).strftime('%d.%m.%Y'),
            #      elem['DisplayBirthDate'],
            #      elem['ConsultantLevelDesc'],
            #      elem['DisplayLastOrderDate'],
            #      elem['DisplayLastOrderRetail'],
            #      elem['Address1'],
            #      elem['Address2'],
            #      elem['Address3'],
            #      elem['Address4'],
            #      elem['Address5'],
            #      elem['AddressCountry'],
            #      elem['FormattedPhoneMobile'],
            #      elem['FormattedPhoneHome'],
            #      elem['FormattedPhoneBusiness'],
            #      elem['EmailAddressPersonal'],
            #      elem['RecruiterConsultantNumber'],
            #      elem['RecruiterLastName'],
            #      elem['RecruiterFirstName'],
            #      # dparser.parse(elem['StartDate'], fuzzy=True).strftime('%d.%m.%Y %H:%M:%S'),
            #      # dparser.parse(elem['LastOrderDate'],fuzzy=True).strftime('%d.%m.%Y %H:%M:%S'),
            #      # elem['LastOrderRetail'],
            #      elem['ConsultantLevelID']])
            # Try changing to xlwt 

            tmpData = (
                ['', elem['UnitNumber'],
                 elem['SubsidiaryCode'],
                 elem['ConsultantID'],
                 elem['LastName'],
                 elem['FirstName'],
                 elem['ConsultantStatus'],
                 dparser.parse(elem['StartDate'], fuzzy=True).strftime('%d.%m.%Y'),
                 elem['DisplayBirthDate'],
                 elem['ConsultantLevelDesc'],
                 elem['DisplayLastOrderDate'],
                 elem['DisplayLastOrderRetail'],
                 elem['Address1'],
                 elem['Address2'],
                 elem['Address3'],
                 elem['Address4'],
                 elem['Address5'],
                 elem['AddressCountry'],
                 elem['FormattedPhoneMobile'],
                 elem['FormattedPhoneHome'],
                 elem['FormattedPhoneBusiness'],
                 elem['EmailAddressPersonal'],
                 elem['RecruiterConsultantNumber'],
                 elem['RecruiterLastName'],
                 elem['RecruiterFirstName'],
                 # dparser.parse(elem['StartDate'], fuzzy=True).strftime('%d.%m.%Y %H:%M:%S'),
                 # dparser.parse(elem['LastOrderDate'],fuzzy=True).strftime('%d.%m.%Y %H:%M:%S'),
                 # elem['LastOrderRetail'],
                 elem['ConsultantLevelID']])

            for kk, finalData in enumerate(tmpData):
                ws.write(ii+1,kk,finalData)

        # print('Writing file to {}'.format(excelPath))
        try:
            wb.save(excelPath)
            # with open(excelPath, 'wb') as f:
            #     f.write(excelData.xls)
            # print('Done...!')
        except:
            print('Error while saving the file to {}'.format(excelPath))
    return True

def inTouchLadderofSuccess(request,user_id):
    import dateutil.parser as dparser

    if not checkCookie('InTouch',user_id):
        return 'error'

    cookieIntouch = load_cookies(os.path.join(getUserPath(user_id),'cookieIntouch.dat'))
    
    with requests.session() as c:

        print('Getting LadderOfSuccessCurrentQuarter.csv')

        try:
            dataTMP = c.get(utils.LadderOfSuccessURL[0], cookies = cookieIntouch)
        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'

        dataJSON = json.loads(dataTMP._content)
        try:
            dataTMP2 = c.get(utils.LadderOfSuccessURL[1], cookies = cookieIntouch)
        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'    
        dataJSON2 = json.loads(dataTMP2._content)

               #Get Ladder of success 
        string1 = '"' + dparser.parse(dataJSON2[0]['Q1BeginDate'], fuzzy=True).strftime('%d.%m.%Y') + ' - ' +\
                        dparser.parse(dataJSON2[0]['Q1EndDate'], fuzzy=True).strftime('%d.%m.%Y') + '","",' + \
                  '"' + dparser.parse(dataJSON2[0]['Q2BeginDate'], fuzzy=True).strftime('%d.%m.%Y') + ' - ' +\
                        dparser.parse(dataJSON2[0]['Q2EndDate'], fuzzy=True).strftime('%d.%m.%Y') + '","",' + \
                  '"' + dparser.parse(dataJSON2[0]['Q3BeginDate'], fuzzy=True).strftime('%d.%m.%Y') + ' - ' +\
                        dparser.parse(dataJSON2[0]['Q3EndDate'], fuzzy=True).strftime('%d.%m.%Y') + '","",' + \
                  '"' + dparser.parse(dataJSON2[0]['Q4BeginDate'], fuzzy=True).strftime('%d.%m.%Y') + ' - ' +\
                        dparser.parse(dataJSON2[0]['Q4EndDate'], fuzzy=True).strftime('%d.%m.%Y') + '",""'


        string2 = '"' + str(int(dataJSON2[0]['Q1ContestAmount'])) + '","' + dataJSON2[0]['Q1LevelAchievedTranslated'] + '",' +\
                  '"' + str(int(dataJSON2[0]['Q2ContestAmount'])) + '","' + dataJSON2[0]['Q2LevelAchievedTranslated'] + '",' + \
                  '"' + str(int(dataJSON2[0]['Q3ContestAmount'])) + '","' + dataJSON2[0]['Q3LevelAchievedTranslated'] + '",' + \
                  '"' + str(int(dataJSON2[0]['Q4ContestAmount'])) + '","' + dataJSON2[0]['Q4LevelAchievedTranslated'] + '"'

        string3 = ''
        for elem in dataJSON:
            tmpSTR = '"' + str(elem['CountryCode']) + '",' + \
                  '"' + "'" +str(elem['ConsultantNumber']) + '",' + \
                  '"' + str(elem['DisplayName']) + '",' + \
                  '"' + str(elem['ConsultantLevelDesc']) + '",' + \
                  '"' + '{}' + '",' + \
                  '"' + '{}' + '",' + \
                  '"' + str(elem['ContestAmountCurrency']) + '",' + \
                  '"' + str(elem['LevelAchievedTranslated']) + '",' + \
                  '"' + str(elem['NeededParameterCreditCurrency'])+ '"' + '\n'
            try:
                NewMemberCount = str(int(elem['NewMemberCount']))
            except:
                NewMemberCount = str((elem['NewMemberCount']))
            try:
                QualifiedMemberCount = str(int(elem['QualifiedMemberCount']))
            except: 
                QualifiedMemberCount = str((elem['QualifiedMemberCount']))

            string3 = string3+tmpSTR.format(NewMemberCount,QualifiedMemberCount)
        
        print('Writing LadderOfSuccessCurrentQuarter.csv ... ')
        try:
            with io.open(os.path.join(getUserPath(user_id),'LadderOfSuccessCurrentQuarter.csv'),'w',encoding='utf8') as f:
                f.write(utils.ladderOfSuccessTMPL.replace('FIRST',string1).replace('SECOND',string2).replace('THIRD',string3))
        except:
            print('An error occured while writing {}'.format(os.path.join(getUserPath(user_id),'LadderOfSuccessCurrentQuarter.csv')))
            return False
        # print(ladderOfSuccessTMPL.replace('FIRST',string1).replace('SECOND',string2).replace('THIRD',string3))

    return True

def inTouchRest(request,user_id,itemNO):

    if not checkCookie('InTouch',user_id):
        return 'error'

    element = utils.url[itemNO]
    jj = itemNO

    cookieIntouch = load_cookies(os.path.join(getUserPath(user_id),'cookieIntouch.dat'))

    BackGroundInput = dict()

    with requests.session() as c:
        try:
            response = c.get(element, cookies = cookieIntouch)
        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'

        soup = BeautifulSoup(response.text, 'lxml')

        for hiddenelem in soup.find('form').find_all('input', type='hidden'):
            BackGroundInput[hiddenelem['id']] = hiddenelem['value']

        utils.payload[jj][soup.find('select')['name']] = \
            soup.find('select').find_all('option', selected='selected')[0]['value']
        # print(soup.find('select').find_all('option', selected='selected')[0]['value'])
        print('Getting information for {}'.format(utils.fileName[jj]))

        BackGroundInput.update(utils.payload[jj])
        try:
            data = c.post(element, data=BackGroundInput, cookies = cookieIntouch)
            print(str(data.status_code))
        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'

        if data.status_code == 200:
            print('Writing file to {}'.format(os.path.join(getUserPath(user_id), utils.fileName[jj])))

            with open(os.path.join(getUserPath(user_id),utils.fileName[jj]), 'wb') as f:
                for chunk in data:
                    f.write(chunk)
            print('Done...!')
        else:
            print('An error occured in downloading file {}'.format(utils.fileName[jj]))

    return True

def unitClubUpload(request,user_id,itemNO):
    import re 
    loginUrl = 'http://www.unit-club.net/up/unitclub/_login.php?'

    payload = {
        'doaction': 'loadnlfile',
        'btnsavenlfile': '   ***   Exportdatei hochladen   ***   ',
    }

    filetype = [
                'ex', # MonthlySales
                'sc', #LadderOfSuccessPreviousQuarter
                'ta', #TeamAnalysisSales
                'tp', #RecruitersCommission                
                'bs', #LadderOfSuccessCurrentQuarter
                'ph', #PeopleAndAdresses
                ]

    # 0 - MonthlySales
    # 1 - LadderOfSuccessPreviousQuarter
    # 2 - TeamAnalysisSales
    # 3 - RecruitersCommission
    # 4 - LadderOfSuccessCurrentQuarter
    # 5 - PeopleAndAdresses

    fileName = [
                'MonthlySales.xls',
                'LadderOfSuccessPreviousQuarter.xls',
                'TeamAnalysisSales.xls',
                'RecruitersCommission.xls',
                'LadderOfSuccessCurrentQuarter.csv',
                'PeopleAndAdresses.xls',
                ]

    print('Item NO: {}'.format(itemNO))
    print(filetype[itemNO])
    print(fileName[itemNO])

    cookieUnitClub = load_cookies(os.path.join(getUserPath(user_id),'cookieUnitClub.dat'))

    with requests.session() as c:

        print('Getting home url')
        try:
            peasoup = BeautifulSoup(c.get('http://www.unit-club.net/up/unitclub/_ps.php?', cookies = cookieUnitClub, timeout=TIMEOUT).text, 'lxml')
            unitLink = peasoup.find('div', id='maintext').find_all('a', title='Zur eigenen Unit-Homepage wechseln')[0]['href']
        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'
        try:
            m = re.search('.*up\/(.*)\/topexchange.*', unitLink)
            print('Trying to get Unit Name First Try')
            print(m.group(1))
        except:
            m = re.search('.*up\/(.*)\/wettbewerb.*', unitLink)
            print('Trying to get Unit Name Second Try')
            print(m.group(1))

        if m:
            found = m.group(1)
        else: 
            print('Could not find home url')
            messages.error(request, 'Dein Unit Name konnte nicht gefunden werdne. Bitte schreibe eine Mail an support@lennartduschek.de!')
            return False

        # prepare the payload indivdual to the corresponding files
        uploadUrl = 'http://www.unit-club.net/up/{}/_nlupload.php?nlupdatelogin={}&upload=1&'.format(found, filetype[itemNO])
        payload['loadfiletype'] = filetype[itemNO]
        payload['nb_load{}'.format(filetype[itemNO])] = 'true'
        print('Uploading {}'.format(fileName[itemNO]))
        filepath = os.path.join(getUserPath(user_id),fileName[itemNO])
        try:
            uploadResponse = c.post(uploadUrl, files={'nlfile': open(filepath, 'rb')}, data=payload, cookies = cookieUnitClub, timeout=TIMEOUT)
            print(str(uploadResponse.status_code))
            # class = infomeldung
            soup = BeautifulSoup(uploadResponse.text, 'lxml')

        except requests.exceptions.Timeout:
            messages.error(request, 'Die Seite reagiert nicht. Bitte versuche \
                                     es später noch einmal oder schreibe eine Mail an support@lennartduschek.de!')
            return 'error'
        
        try:
            mydivs = soup.findAll("div", { "class" : "infomeldung" })
            print(mydivs)
        except:
            print('Abschnitt infomeldung nicht gefunden!')

        if 'erfolgreich geladen!' in soup.text:
            print('erfolgreich geladen!')

        print('Closing up the Unit Club Session!')
        c.close()

        # for i, number in enumerate(filetype):
        #     uploadUrl = 'http://www.unit-club.net/up/{}/_nlupload.php?nlupdatelogin={}&upload=1&'.format(found, number)
        #     # print(response.headers)
        #     payload['loadfiletype'] = number
        #     payload['nb_load{}'.format(number)] = 'true'

        #     print('Uploading {}'.format(fileName[i]))
        #     uploadResponse = c.post(uploadUrl, files={'nlfile': open(fileName[i], 'rb')}, data=payload, cookies = cookieUnitClub)

        #     print(str(uploadResponse.status_code))
        #     soup = BeautifulSoup(uploadResponse.text, 'lxml')
        #     if 'erfolgreich geladen!' in soup.text:
        #         print('erfolgreich geladen!')

        #     del payload['nb_load{}'.format(number)]

        # print('Closing up the Unit Club Session!')


        # c.close()

def checkCookie(type,user_id):
    
    if type == "InTouch":
        if os.path.isfile(os.path.join(getUserPath(user_id),'cookieIntouch.dat')):
            return True
        else: 
            return False
    elif type == "UnitClub":
        if os.path.isfile(os.path.join(getUserPath(user_id),'cookieUnitClub.dat')):
            return True
        else: 
            return False
    else:
        return False

def save_cookies(requests_cookiejar, filename):
    import pickle
    with open(filename, 'wb') as f:
        pickle.dump(requests_cookiejar, f)

def load_cookies(filename):
    import pickle
    with open(filename, 'rb') as f:
        return pickle.load(f)

def getUserPath(user_id):
    return os.path.join(settings.BASE_DIR,'updater','files',str(user_id))

